#! /usr/bin/perl -w

use Modern::Perl;

use Mana;
use Test::More tests => 4;
use Plack::Test;
use HTTP::Request::Common;
use Dancer2;
use Dancer2::Plugin::Database;
use Dancer::Logger;
use Data::Dumper;
use Dancer2::Plugin::REST;
use Dancer2::Serializer::JSON;
use utf8;

my $app = Mana->to_app;

my $test = Plack::Test->create($app);

database->{AutoCommit} = 0;
database->quick_delete("review", 1);
database->quick_delete("librarian", 1);
database->quick_insert("librarian", { id => "1234", email =>'xx@xx.com', firstname => 'Pierre', lastname => 'Dupont', creationdate => '2017-04-18', activationdate => '2017-04-18'});



my $newReview;
$newReview->{review}="sapu";
$newReview->{reviewid}="12";
$newReview->{documentid}="123456789";
$newReview->{idtype}="isbn";
$newReview->{language}="fr-FR";
my $newReviewShort;
%$newReviewShort = %$newReview;
$newReview->{securitytoken}="1234";

my @attributelist = keys %$newReview;


subtest 'critical cases for Share function' => sub {
    plan tests => 5;

    my $temp = delete $newReview->{review};
    my $newJsonReview = to_json($newReview, { utf8 => 1 });
    my $nbbeforeinsert = database->quick_count("review", 1);
    my $resJson = $test->request( POST '/review.json', Content => $newJsonReview );
    my $res = from_json( $resJson->content );

    subtest 'attribute review missing' => sub {
        plan tests => 3;
        is( $resJson->code, 406, "right error code");
        ok( $res->{msg} =~ /Invalid input on/, "propper error message");

        #nothing were added
        is( database->quick_count( "review", 1 ), $nbbeforeinsert, "database is the same after error");
    };

    $newReview->{review} = $temp;
    $temp = delete $newReview->{documentid};
    $newJsonReview = to_json($newReview, { utf8 => 1 });
    $nbbeforeinsert = database->quick_count("review", 1);
    $resJson = $test->request( POST '/review.json', Content => $newJsonReview );

    subtest 'attribute documentid missing' => sub {
        is( $resJson->code, 406, "right error code");
        $res = from_json( $resJson->content );
        ok( $res->{msg} =~ /Invalid input on/, "propper error message");

        #nothing were added
        is( database->quick_count( "review", 1 ), $nbbeforeinsert, "database is the same after error");
    };
    $newReview->{documentid} = $temp;

    $temp = delete $newReview->{language};
    $newJsonReview = to_json($newReview, { utf8 => 1 });
    $nbbeforeinsert = database->quick_count("review", 1);
    $resJson = $test->request( POST '/review.json', Content => $newJsonReview );

    subtest 'attribute language missing' => sub {
        is( $resJson->code, 406, "right error code");
        $res = from_json( $resJson->content );
        ok( $res->{msg} =~ /Invalid input on/, "propper error message");

        #nothing were added
        is( database->quick_count( "review", 1 ), $nbbeforeinsert, "database is the same after error");
    };
    $newReview->{language} = $temp;

    $temp = delete $newReview->{reviewid};
    $newJsonReview = to_json($newReview, { utf8 => 1 });
    $nbbeforeinsert = database->quick_count("review", 1);
    $resJson = $test->request( POST '/review.json', Content => $newJsonReview );

    subtest 'attribute reviewid missing' => sub {
        is( $resJson->code, 406, "right error code");
        $res = from_json( $resJson->content );
        ok( $res->{msg} =~ /Invalid input on/, "propper error message");

        #nothing were added
        is( database->quick_count( "review", 1 ), $nbbeforeinsert, "database is the same after error");
    };
    $newReview->{reviewid} = $temp;

    $temp = delete $newReview->{idtype};
    $newJsonReview = to_json($newReview, { utf8 => 1 });
    $nbbeforeinsert = database->quick_count("review", 1);
    $resJson = $test->request( POST '/review.json', Content => $newJsonReview );

    subtest 'attribute idtype missing' => sub {
        is( $resJson->code, 406, "right error code");
        $res = from_json( $resJson->content );
        ok( $res->{msg} =~ /Invalid input on/, "propper error message");

        #nothing were added
        is( database->quick_count( "review", 1 ), $nbbeforeinsert, "database is the same after error");
    };
    $newReview->{idtype} = $temp;
};


subtest 'Share function' => sub {
    plan tests => 3;


    my $newJsonReview = to_json($newReview, { utf8 => 1 });
    my $nbbeforeinsert = database->quick_count("review", 1);
    my $resJson = $test->request( POST '/review.json', Content => $newJsonReview );
    my $resdb = database->quick_select("review", $newReviewShort);
    my $nbafterinsert = database->quick_count("review", 1);
    is( $nbafterinsert, $nbbeforeinsert + 1, "added something in database");


    foreach my $key ( keys %{$resdb} ){
        delete $resdb->{$key} unless grep {$key eq $_} @attributelist;
    }



        #test about expected values of the fields
    is_deeply( $resdb, $newReview,  "correctly added");

        #test about the error returned by function
    my $res = from_json( $resJson->content );
    is( $res->{'error'}, undef , "no error returned");
};



subtest "Get by id Route" => sub {
    plan tests => 2;
    my $id = database->quick_select("review", $newReviewShort)->{id};

    my $resJson = $test->request( GET "/review/$id.json" );
    my $res = from_json( $resJson->content );
    my $resdata = $res->{data};

    foreach my $key ( keys %{$resdata} ){
        delete $resdata->{$key} unless grep {$key eq $_} @attributelist;
    }

        #test about expected values of the fields
    $resdata->{securitytoken} = $newReview->{securitytoken};
    is_deeply( $resdata, $newReview,  "get the right resource with id");

        #test about the error returned by function
    is( $res->{'error'}, undef , "no error returned");
 
};

subtest "Search engine" => sub {
    plan tests => 3;
 
    my $newReview2;
    $newReview2->{review}="sapu";
    $newReview2->{reviewid}="13";
    $newReview2->{documentid}="no_id";
    $newReview2->{idtype}="isbn";
    $newReview2->{language}="fr-FR";
    $newReview2->{securitytoken}="1234";

    my $newReview3;
    $newReview3->{review}="sapu";
    $newReview3->{reviewid}="14";
    $newReview3->{documentid}="123456789";
    $newReview3->{idtype}="blurp";
    $newReview3->{language}="fr-FR";
    $newReview3->{securitytoken}="1234";



    my $newJsonReview2 = to_json($newReview2, { utf8 => 1 });
    my $jsonresult2 = $test->request( POST '/review.json', Content => $newJsonReview2 );
    my $result = from_json($jsonresult2->content);
    my $id = database->quick_select("review", $newReviewShort)->{id};

    my $newJsonReview3 = to_json($newReview3, { utf8 => 1 });
    $test->request( POST '/review.json', Content => $newJsonReview3 );

    my $resJson = $test->request( GET "/review.json?idtype=isbn&documentid=123456789");
    my $res = from_json( $resJson->content );
    my $resdata = $res->{data};


    foreach my $key ( keys %{$resdata->[0]} ){
        delete $resdata->[0]->{$key} unless grep {$key eq $_} @attributelist;
    }

    is_deeply($resdata->[0], $newReview, "research successful");
    is(scalar keys @$resdata, 1, "only the expected search");
        #test about the error returned by function
    is( $res->{'error'}, undef , "no error returned");
 
};

eval{ database->rollback; };
database->{AutoCommit} = 1;
1;
