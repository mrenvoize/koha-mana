#! /usr/bin/perl -w

use Modern::Perl;

use Mana;
use Test::More tests => 5;
use Plack::Test;
use HTTP::Request::Common;
use Dancer2;
use Dancer2::Plugin::Database;
use Dancer::Logger;
use Data::Dumper;
use Dancer2::Plugin::REST;
use Dancer2::Serializer::JSON;
use utf8;

my $app = Mana->to_app;

my $test = Plack::Test->create($app);

database->{AutoCommit} = 0;
database->quick_delete("report", 1);
database->quick_insert("librarian", { id => 1234, email => 'xx.xx@xx.com', firstname => 'Nathalie', lastname => 'Portman', creationdate => '2017-04-19', activationdate => '2017-04-19'});

my $newReport;
$newReport->{savedsql} ="Select pommes from arbre" ;
$newReport->{report_name} = "verger" ;
$newReport->{report_group} = "CiderBrew" ;
$newReport->{notes} = "Ceci compte les pommes du verger";
$newReport->{type} = "pommesCounter";
$newReport->{securitytoken} = 1234;
$newReport->{language} = "FR";
my @attributelist = keys %$newReport;


subtest 'critical cases for Share function' => sub {
    plan tests => 5;
    
    my $temp = delete $newReport->{savedsql};
    my $newJsonReport = to_json($newReport, { utf8 => 1 });
    my $nbbeforeinsert = database->quick_count("report", 1);
    my $resJson = $test->request( POST '/report.json', Content => $newJsonReport );
    my $res = from_json( $resJson->content );

    subtest 'attribute savedsql missing' => sub {
        plan tests => 3;
        is( $resJson->code, 406, "right error code");
        ok( $res->{msg} =~ /Invalid input on/, "propper error message");

        #nothing were added
        is( database->quick_count( "report", 1 ), $nbbeforeinsert, "database is the same after error");
    };

    $newReport->{savedsql} = $temp;

    $temp = delete $newReport->{report_name};
    $newJsonReport = to_json($newReport, { utf8 => 1 });
    $nbbeforeinsert = database->quick_count("report", 1);
    $resJson = $test->request( POST '/report.json', Content => $newJsonReport );

    subtest 'attribute report_name missing' => sub {
        is( $resJson->code, 406, "right error code");
        $res = from_json( $resJson->content );
        ok( $res->{msg} =~ /Invalid input on/, "propper error message");

        #nothing were added
        is( database->quick_count( "report", 1 ), $nbbeforeinsert, "database is the same after error");
    };
    $newReport->{report_name} = $temp;

    $temp = delete $newReport->{report_group};
    $newJsonReport = to_json($newReport, { utf8 => 1 });
    $nbbeforeinsert = database->quick_count("report", 1);
    $resJson = $test->request( POST '/report.json', Content => $newJsonReport );

    subtest 'attribute report_group missing' => sub {
        is( $resJson->code, 406, "right error code");
        $res = from_json( $resJson->content );
        ok( $res->{msg} =~ /Invalid input on/, "propper error message");

        #nothing were added
        is( database->quick_count( "report", 1 ), $nbbeforeinsert, "database is the same after error");
    };
    $newReport->{report_group} = $temp;

    $temp = delete $newReport->{notes};
    $newJsonReport = to_json($newReport, { utf8 => 1 });
    $nbbeforeinsert = database->quick_count("report", 1);
    $resJson = $test->request( POST '/report.json', Content => $newJsonReport );

    subtest 'attribute notes missing' => sub {
        is( $resJson->code, 406, "right error code");
        $res = from_json( $resJson->content );
        ok( $res->{msg} =~ /Invalid input on/, "propper error message");

        #nothing were added
        is( database->quick_count( "report", 1 ), $nbbeforeinsert, "database is the same after error");
    };
    $newReport->{notes} = $temp;

    $temp = delete $newReport->{language};
    $newJsonReport = to_json($newReport, { utf8 => 1 });
    $nbbeforeinsert = database->quick_count("report", 1);
    $resJson = $test->request( POST '/report.json', Content => $newJsonReport );

    subtest 'attribute language missing' => sub {
        is( $resJson->code, 406, "right error code");
        $res = from_json( $resJson->content );
        ok( $res->{msg} =~ /Invalid input on/, "propper error message");

        #nothing were added
        is( database->quick_count( "report", 1 ), $nbbeforeinsert, "database is the same after error");
    };
    $newReport->{language} = $temp;

};

subtest 'Share function' => sub {
    plan tests => 7;


    my $newJsonReport = to_json($newReport, { utf8 => 1 });
    my $nbbeforeinsert = database->quick_count("report", 1);
    my $resJson = $test->request( POST '/report.json', Content => $newJsonReport );
    my $resdb = database->quick_select("report", $newReport);
    my $nbafterinsert = database->quick_count("report", 1);
    is( $nbafterinsert, $nbbeforeinsert + 1, "added something in database");

        #test about expected values of the fields
    is( $resdb->{savedsql}, "Select pommes from arbre","sql correctly loaded in mana KB");
    is( $resdb->{report_name},"verger", "report name correctly loadad in mana KB");
    is( $resdb->{notes}, "Ceci compte les pommes du verger","notes correctly added");
    is( $resdb->{type}, "pommesCounter", "type correctly added");
    is( $resdb->{nbofusers}, 1,  "nb of users corrctly loaded");

        #test about the error returned by function
    my $res = from_json( $resJson->content );
    is( $res->{'error'}, undef , "no error returned");
   
};

subtest "Get by id Route" => sub {
    plan tests => 6;
    my $id = database->quick_select("report", $newReport)->{id};

    my $resJson = $test->request( GET "/report/$id.json" );
    my $res = from_json( $resJson->content );
    my $resdata = $res->{data};


        #test about expected values of the fields
    is( $resdata->{savedsql}, "Select pommes from arbre","sql correctly loaded from mana KB");
    is( $resdata->{report_name},"verger", "report name correctly loaded from mana KB");
    is( $resdata->{notes}, "Ceci compte les pommes du verger","notes correctly loaded");
    is( $resdata->{type}, "pommesCounter", "type correctly added");
    is( $resdata->{nbofusers}, 1,  "nb of users correctly loaded");

        #test about the error returned by function
    is( $res->{'error'}, undef , "no error returned");
 
};

subtest "Search engine" => sub {

    plan tests => 3;
    my $newReport2;
    $newReport2->{savedsql} ="Select voiture from parking" ;
    $newReport2->{report_name} = "parking" ;
    $newReport2->{report_group} = "Carparker" ;
    $newReport2->{notes} = "Count cars from parking";
    $newReport2->{type} = "carsCounter";
    $newReport2->{securitytoken} ="1234" ;
    $newReport2->{language} ="NL" ;

    my $newReport3;
    $newReport3->{savedsql} ="Select bird from sky" ;
    $newReport3->{report_name} = "sky" ;
    $newReport3->{report_group} = "skyseer" ;
    $newReport3->{notes} = "Count birds from sky";
    $newReport3->{type} = "birdsCounter";
    $newReport3->{securitytoken} ="1234" ;
    $newReport3->{language} ="FR" ;



    my $newJsonReport2 = to_json($newReport2, { utf8 => 1 });
    $test->request( POST '/report.json', Content => $newJsonReport2 );
    my $newJsonReport3 = to_json($newReport3, { utf8 => 1 });
    my $result = $test->request( POST '/report.json', Content => $newJsonReport3 );

 
    my $resJson = $test->request( GET "/report.json?query=bird");
    my $res = from_json( $resJson->content );
    my $resdata = $res->{data};



    foreach my $key ( keys %{$resdata->[0]} ){
        delete $resdata->[0]->{$key} unless grep {$key eq $_} @attributelist;
    }


    is_deeply($resdata->[0], $newReport3, "research successful");
    is(scalar keys @$resdata, 1, "only the expected search");
        #test about the error returned by function
    is( $res->{'error'}, undef , "no error returned");
 
};

subtest "increment field" => sub {
    plan tests => 2;
    
    my $id = database->quick_select("report", $newReport)->{id};
    my $resJson = $test->request( POST "/report/$id.json/increment/nbofusers?securitytoken=".$newReport->{securitytoken} );


    my $incrementedReportInDB = database->quick_select("report", { id => $id } );
    is( $incrementedReportInDB->{nbofusers}, 2, "correctly incremented" ); 

    $resJson = $test->request( POST "/report/$id.json/increment/nbofusers?step=2&securitytoken=".$newReport->{securitytoken});
    $incrementedReportInDB = database->quick_select("report", { id => $id } );
    is( $incrementedReportInDB->{nbofusers}, 4, "correctly incremented" );

};

eval{ database->rollback; };
database->{AutoCommit} = 1;
1;
