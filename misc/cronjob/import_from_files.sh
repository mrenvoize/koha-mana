#!/bin/sh

DIRECTORIES=/home/koha/koha-mana/data/*
CONFIG=/home/koha/etc/koha-patron-staff-conf.yaml

for d in $DIRECTORIES
do
    DIRECTORY=$d"/*"
    RESOURCENAME=${d##*/}
    if [ -d $d ]&&[ "$RESOURCENAME" != "raw" ];
    then
     for f in $DIRECTORY
     do
         FILENAME=${f##*/}
         if [ ! -d $f ]&&[ -f $f ]
         then
             PLACK_ENV=production perl /home/koha/koha-mana/scripts/import_resource.pl -r $RESOURCENAME -f $f -l /home/koha/koha-mana/logs/import.log
             mv $f $d"/old/"
             gzip  -f $d"/old/"$FILENAME
         fi
     done
     fi
done

find /home/koha/koha-mana/data -maxdepth 2 -type f -mtime +20 -delete
