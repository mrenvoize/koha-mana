DROP TABLE if exists review;
CREATE TABLE review (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `review` text NOT NULL,
  `reviewid` int NOT NULL,
  `documentid` varchar(20) NOT NULL,
  `idtype` varchar(10) NOT NULL,
  `creationdate` date NOT NULL,
  `exportemail` varchar(255) DEFAULT NULL,
  `kohaversion` varchar(255) DEFAULT NULL,
  `language` varchar(255) NOT NULL,
  `securitytoken` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=290 DEFAULT CHARSET=utf8mb4;

ALTER TABLE review ADD INDEX review ( review(10)), ADD INDEX documentid( documentid(15) ), ADD INDEX idtype(idtype); 
