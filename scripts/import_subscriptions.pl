#!/usr/bin/perl

# This file is part of Koha.
#
# Koha is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Koha is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Koha; if not, see <http://www.gnu.org/licenses>.

=head1 NAME

import_subscriptions.pl - converts a Koha base to a Mana Base

=head1 SYNOPSIS

./ import_subscription.pl -l fr-FR [resource][source file]

=head1 DESCRIPTION

The script imports datas in mana KB from a csv file.

=cut

use Modern::Perl;
use Getopt::Long;
use Pod::Usage;
use Text::CSV;

use Dancer2;
use Dancer2::Plugin::Database;
use DateTime;
use Time::HiRes;


use Mana::Resource;

my ($help, $language, $verbose);
GetOptions(
    'help|?'        => \$help,
    'language|l:s'  => \$language,
    'verbose|v'     => \$verbose,
) or pod2usage(2);

$language ||= 'en';

my $cter = 0;
my $progress = 0;
my $row;
my $nbprocessed;
my $resource = "subscription";
my $existing;
my $begin = DateTime->now;
my $beginns = Time::HiRes::time();

my $csv = Text::CSV->new({ binary=>1, allow_loose_quotes=>1, escape_char=>"\\"});
foreach my $currentcsv (@ARGV){
    print "current file getting imported: $currentcsv\n";
    print "Nb of lines before insert:".database->quick_count($resource, 1)."\n";
    open(my $fh, '<:encoding(UTF-8)', $currentcsv) or die "Could not open file '$ARGV[0]'";
    my $length = scalar @{ $csv->getline_all($fh) } - 1;
    print "length of the file: $length"."\n";
    close $fh;
    open( $fh, '<:encoding(UTF-8)', $currentcsv) or die "Could not open file '$ARGV[0]'";
    $csv->column_names( $csv->getline( $fh ) );
    $cter = 0;
    $progress = 0;
    while (  $row = $csv->getline_hr( $fh ) ) {
        $cter += (100 / $length);
        if ( $cter - $progress > 5 ){
            $nbprocessed = int ($cter*$length/100);
            print "$nbprocessed lines on $length processed\n";
            $progress = $cter;
        }

        $existing = database->quick_select($resource, { issn => $row->{issn}, language => $row->{language} }, { columns => "id"});
        $row->{exportemail} = $currentcsv;
        $row->{creationdate} = DateTime->now->ymd;
        #we don't use dedoubling functions on insert like PostEntity to face the fact some datas from libraries are inconsistent
        my $resourceLanguage = $row->{ language };
        $resourceLanguage ||=$language;
        if ( (not $existing) and $row->{numberingmethod} and $row->{unit} and $row->{unitsperissue} and $row->{issuesperunit} ){
           database->quick_insert( $resource, $row );
        }
    }
    close $fh;
    print "Nb of lines after insert:".database->quick_count($resource, 1);
}

#time tool
my $duration = DateTime->now - $begin;
my $durationns = (Time::HiRes::time() - $beginns);
$durationns = $durationns - int $durationns;
$durationns = int $durationns * 1000000;
print "Finished, total duration: ".$duration->hours."h ".$duration->minutes."min ".$duration->seconds."s ".$durationns."ns\n";
