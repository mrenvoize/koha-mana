package Mana::Resource::Resource_comment;

use Dancer2;
use Dancer2::Plugin::Database;
use DateTime;
use Mana::Resource;
use vars qw( @ISA ); @ISA = qw( Mana::Resource );



#require Mana::Resource;

our @valid_params = (
    'resource_type', 'resource_id'
);

sub getValidParams { return @valid_params;}

our @required_entries = (
    'message', 'resource_id', 'resource_type'
);

sub getValidEntries { return @required_entries;}

our $resource_name = 'resource_comment';
sub getResourceName { return $resource_name };

sub dataInit(){
    my $self = shift;
    my $content = shift;
    $content->{creationdate} = DateTime->now->ymd; 
    $content->{nb} = 1; 
    return $content;
}

sub isValidInput {
    my ($self, $content) = @_;
    my @keys = keys %$content;
    foreach my $required_entry (@required_entries){
        unless ( grep {$_ eq $required_entry} @keys ){
            return 0;
        }
    }
    if ( Mana::Resource::isValidResource($content->{resource_type})){
        eval {database->quick_select( $content->{resource_type}, { id => $content->{resource_id} })} and return 1; ;
    }
    return 0;
}


sub resourceAlreadyExists {
    my ($self, $content) = @_;
    my $bulk_import = delete $content->{bulk_import};
    my $content_to_check;
    $content_to_check->{message} = $content->{message};
    $content_to_check->{resource_type} = $content->{resource_type};
    $content_to_check->{resource_id} = $content->{resource_id};
    if ( my $existing = database->quick_select('resource_comment', $content_to_check) ){
        return $existing;
    }
}

sub destructor {
    return 1;
}

1;
