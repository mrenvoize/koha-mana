package Mana::Resource::Librarian;

use Modern::Perl;
use Dancer2;
use Dancer2::Plugin::Database;
use Mana::Resource;
require Exporter;
use vars qw( @ISA ); @ISA = qw( Mana::Resource );
use constant ANONYMOUS_TOKEN => "ANONYMOUS_TOKEN";

sub getResourceName{ return "librarian"; }

sub getValidParams{ return; }

sub destructor{
    my ($self, $securitytoken) = @_;
    foreach my $resource ( Mana::Resource->getValidResource ){
        database->quick_update( $resource, { securitytoken => $securitytoken }, { securitytoken => "ANONYMOUS_TOKEN" } );
    }
}

1;
