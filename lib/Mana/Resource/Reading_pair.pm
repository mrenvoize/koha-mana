package Mana::Resource::Reading_pair;

use Dancer2;
use Dancer2::Plugin::Database;
use Mana::Resource;
use Business::ISBN;

use vars qw( @ISA ); push @ISA, "Mana::Resource";

use Modern::Perl;

our @valid_params = (
    'documentid1', 'documentid2', 'idtype1', 'idtype2'
);

sub getValidParams {
    return @valid_params;
}

our @required_entries  = (
    'documentid1', 'documentid2', 'idtype1', 'idtype2'
);

sub getValidEntries {
    return @required_entries;
}

our $resource_name = 'reading_pair';

sub getResourceName { return $resource_name; };

sub isValidInput {
    my ($self, $content) = @_;
    my @keys = keys %$content;

    foreach my $required_entry ( $self->getValidEntries() ){
        unless ( grep {$_ eq $required_entry} @keys ){
            return 0;
        }
    }
    return 0 unless ( $content->{documentid1} );
    return 0 unless ( length $content->{documentid1} < 15 );
    return 0 unless ( $content->{documentid2} );
    return 0 unless ( length $content->{documentid2} < 15 );
    return 1;
}

sub dataInit {
    my $self = shift;
    my $content = shift;
    $content->{nb} = 1 unless $content->{nb};


    #check if ean can be converted in isbn
    if ( $content->{idtype1} eq "ean"){
        my $isbntry = Business::ISBN->new( $content->{documentid1} );
        if( $isbntry and $isbntry->is_valid ){
            $content->{idtype1} = "isbn";
            $content->{documentid1} = $isbntry->as_isbn13->as_string([]);
        }
    }
    if ( $content->{idtype2} eq "ean"){
        my $isbntry = Business::ISBN->new( $content->{documentid2} );
        if( $isbntry and $isbntry->is_valid ){
            $content->{idtype2} = "isbn";
            $content->{documentid2} = $isbntry->as_isbn13->as_string([]);
        }
    }


    if ( ($content->{documentid1} cmp $content->{documentid2}) == 1){
        my $temp;
        $temp = $content->{documentid1};
        $content->{documentid1}=$content->{documentid2};
        $content->{documentid2}=$temp;
        $temp = $content->{idtype1};
        $content->{idtype1}=$content->{idtype2};
        $content->{idtype2}=$temp;
    }
    $content->{nb_this_month} = 1 unless $content->{nb};
    return $content;
}

sub resourceAlreadyExists {
    my ($self, $content) = @_;
    my $bulk_import = delete $content->{bulk_import};
    my $content_to_check;

    if ( ($content->{documentid1} cmp $content->{documentid2}) == 1){
        $content_to_check->{documentid1}=$content->{documentid2};
        $content_to_check->{documentid2}=$content->{documentid1};
        $content_to_check->{idtype1}=$content->{idtype2};
        $content_to_check->{idtype2}=$content->{idtype1};
    }
    else{
        $content_to_check->{documentid2}=$content->{documentid2};
        $content_to_check->{documentid1}=$content->{documentid1};
        $content_to_check->{idtype1}=$content->{idtype1};
        $content_to_check->{idtype2}=$content->{idtype2};
    }
    if ( $content_to_check->{idtype1} eq "ean"){
        my $isbntry = Business::ISBN->new( $content_to_check->{documentid1} );
        if( $isbntry and $isbntry->is_valid ){
            $content_to_check->{idtype1} = "isbn";
            $content_to_check->{documentid1} = $isbntry->as_string([]);
        }
    }
    if ( $content_to_check->{idtype2} eq "ean"){
        my $isbntry = Business::ISBN->new( $content_to_check->{documentid2} );
        if( $isbntry and $isbntry->is_valid ){
            $content_to_check->{idtype2} = "isbn";
            $content_to_check->{documentid2} = $isbntry->as_string([]);
        }
    }


    if ( my $existing = database->quick_select('reading_pair', $content_to_check) ) {

        return $existing->{id};
    }
    else {
        return 0;
    }
}

sub specificSearch {
    my ($self, $params) = @_;
    my $resourceName = $self->getResourceName();
    my $query = "SELECT * FROM $resourceName WHERE 1";

    my @keys;
    my @values;
    if ( ($params->{documentid1} cmp $params->{documentid2}) == 1){
        my $temp = $params->{documentid1};
        $params->{documentid1} = $params->{documentid2};
        $params->{documentid2} = $temp;

        $temp = $params->{idtype1};
        $params->{idtype1} = $params->{idtype2};
        $params->{idtype2} = $temp;
    }
    my $filtered_params = $self->filterParamsForSearch($params);
    while ( my ($key, $value) = each(%$filtered_params) ) {
        if (defined $value) {
            my @words = split / /, $value;
            foreach my $word (@words) {
                push @keys, $key." = ?";
                push @values, "$word";
            }
        }
    }

    if ( scalar @keys ne 0 ) {
        $query .= " AND " . join(" AND ", @keys);
    }
    my $sth = database->prepare($query);
    $sth->execute(@values);
    my $rows = $sth->fetchall_arrayref({});
    return $rows;
}



sub getSuggestion {
    my ( $self, $id, $idtype, $offset, $nb ) = @_;
    $nb||=10;
    $offset||=1;
    my @list1 = database->quick_select(
        'reading_pair',
         {
             documentid2 => $id,
             idtype2 => $idtype
         },
         {
             columns => [ qw( documentid1 idtype1 nb ) ],
             order_by => { desc => 'nb'},
             limit => $nb + $offset
         } );
    my @list2 = database->quick_select(
        'reading_pair',
         {
             documentid1 => $id,
             idtype1 => $idtype
         },
         {
             columns => [ qw( documentid2 idtype2 nb ) ],
             order_by => { desc => 'nb'},
             limit => $nb + $offset
         } );

    foreach my $document ( @list2 ){
        $document->{ documentid } = delete $document->{ documentid2 };
        $document->{ idtype } = delete $document->{ idtype2 };
    }
    foreach my $document ( @list1 ){
        $document->{ documentid } = delete $document->{ documentid1 };
        $document->{ idtype } = delete $document->{ idtype1 };
    }

    if ( @list1 and @list2 ){
        my @result = _myMerge( \@list1, \@list2, $offset, $nb );

        return \@result;
    }
    return \@list2 if ( @list2 );
    return \@list1;
}

sub _myMerge {
    my ( $list1, $list2, $offset, $maxsize ) = @_;
    $maxsize or $maxsize = 10;
    $offset or $offset = 1;
    my @result;
    my $arg2 = shift @$list2;
    my $arg1 = shift @$list1;
    my $cter = 1;
    while ( defined $arg1 and defined $arg2 and $maxsize+$offset != $cter ) {
        $cter++;
        if ( $arg2->{nb} > $arg1->{nb} ){
           push @result, $arg2;
           $arg2 = shift @$list2;
        }
        else {
           push @result, $arg1;
          $arg1 = shift @$list1;
        }
    }
    unless ( $cter == $maxsize + $offset){
        push @result, $arg1 if $arg1;
        push @result, $arg2 if $arg2;
        push @result, @$list1 if $arg1;
        push @result, @$list2 if $arg2;
    }
    my $cnt = -1;
    @result = grep {
        ++$cnt >= $offset - 1
        and $cnt < $maxsize + $offset - 1
        and $result[ $cnt ]
    } @result;
    return @result;
}

1;
