=head1 NAME

General comments on mana KB code

=head2 General description

=head3 Code

The entry point of the api is the page lib/Mana.pm, it describes all possibles routes to call Mana.
The rest of the perl code (Perl Modules) is in lib/Mana/.
The templates are stored in views/

=head3 Route

When a route is called, the code in Mana.pm is processed.
They usualy just call a function in lib/Mana or call for a template in views.
However, a hook is called before and another after, whatever the route is called.

=head2 Elements of the code.

=head3 Hooks

=head4 Hook before.

The hook before checks:

=over

=item

If the method is POST, it checks that the sender is legit, threw the presence of a security token (see L<Security token>).
It returns a status error 403 (unauthorized) in case of fail.

=item

If the method is concerning a resource, it checks that the resource exists and can be handled by Mana
It returns a status error 400 (Bad request) in case of fail.

=back

=head4 Hook after

The hook after is in charge of giving header the good status code.
Since it's impossible to change the status code in the librairies themselves ( directly correlated with the behaviour of Dancer2 ), we have to set them in the main module: Mana.pm. This is done in the hook after. It takes the statuscode returned by the body of the route and set the propper status code in the header.

=head3 Routes

=head4 Authentication

There are 2 levels of authentication on Mana. The administration authentication level and the user authentication.

=head4 Administration

To see the whole informations or to remove datas from the database, a user has to be logged in. To log in, there is an interface on the routes /login (GET and POST). This login is useful for all routes beginning with /admin. For now, all resources which can be browsed are Subscriptions and Reports.
All accounts are stored in the database in table users.
To create an administration account, you have to create a field directly in the Database, passwords are generated with the B-Crypt algorithm (see Dancer2::Plugin::Passphrase.
To remove an administration account, you have to remove it directly in the Database.

=head4 Security token

To Post datas on Mana, you have to send in the body of the request a security token of 32 printable characters.
To get a security token you have to use the routes /getsecuritytoken and /registration, which invite you to prove you are human. If the process is successful, the user will get a randomly generated token.
All token are stored in the database in table librarians.
To delete a security token, you have to remove it directly in the database.

=head4 Interface.

All function used in the interface are in lib/Mana/Interface.pm.
The interface is called with all route beginning with /admin and that's why the interface is reserved to administrators.
Interface is useful to browse, see all details on a resource, or to remove some datas.

=head4 API

The api is composed of all other routes. They are mainly related to one resource and usualy begin with /:resource/ or at least contain :resource.
They usualy just call a generic function in lib/Mana/Resource.pm.
This generic function has a general comportement and calls specific functions from the package lib/Mana/Resource/myresource.pm when it is necessary.
The package Resource.pm is as general as possible and calls functions for specific contents.
The packages in lib/Mana/Resource/ contain all specific content. They allway have the same interface.
